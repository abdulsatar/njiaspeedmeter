package com.example.abdulsataraboud.njiaspeedmeter;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by abdulsataraboud on 4/24/18.
 */

public class SendData {

    /* Class for sending Speed and location data to Njia server */

    private String url = "http://138.68.234.54/njia/controller/testPush.php";
    private RequestQueue requestQueue;

    public SendData() {

    }

    public void sendData(final String latitude, final String longitude, final String speed, Context context) {
        requestQueue = Volley.newRequestQueue(context.getApplicationContext());

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> data = new HashMap<String, String>();
                data.put("latitude", latitude);
                data.put("longitude", longitude);
                data.put("speed",speed);

                return data;
            }
        };

        requestQueue.add(stringRequest);

    }
}
