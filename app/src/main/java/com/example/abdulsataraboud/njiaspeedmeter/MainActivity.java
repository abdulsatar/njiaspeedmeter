package com.example.abdulsataraboud.njiaspeedmeter;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import static android.R.attr.handle;
import static android.os.Build.VERSION_CODES.M;
import static com.example.abdulsataraboud.njiaspeedmeter.R.id.speed;

public class MainActivity extends AppCompatActivity {

    private FusedLocationProviderClient mFusedLocationClient;
    private TextView userLocation, userSpeed;
    private static final int GRANT_PERMISSIONS = 101;
    private LocationRequest locationRequest;
    private LocationCallback locationCallback;
    private SendData sendData;
    private String latitude="0", longitude="0", speed="0";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        userLocation = (TextView) findViewById(R.id.location);
        userSpeed = (TextView) findViewById(R.id.speed);


        locationRequest = new LocationRequest();
        locationRequest.setInterval(5000); // 5000 milliseconds = 5 seconds
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY); // Use GPS location
        
        //Sending data to server class
        sendData = new SendData();

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            mFusedLocationClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {

                    if (location != null) {

                        latitude = String.valueOf(location.getLatitude());
                        longitude = String.valueOf(location.getLongitude());
                        userLocation.setText(latitude + ", " + longitude);
                        

                        if (location.hasSpeed()) {
                            speed = String.valueOf(location.getSpeed());
                            userSpeed.setText(speed + " m/s");
                        }

                        //Sending user captured data to Remote server
                        sendData.sendData(latitude,longitude,speed,MainActivity.this);
                        
                    } else {
                        Toast.makeText(MainActivity.this, "There is no current location", Toast.LENGTH_LONG);
                    }
                }
            });
        } else {
            // Request Device for permission

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, GRANT_PERMISSIONS);
            }

        }

        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                for (Location location : locationResult.getLocations()) {
                    // Update UI with location data
                    if (location != null) {

                        latitude = String.valueOf(location.getLatitude());
                        longitude = String.valueOf(location.getLongitude());
                        userLocation.setText(latitude + ", " + longitude);


                        if (location.hasSpeed()) {
                            speed = String.valueOf(location.getSpeed());
                            userSpeed.setText(speed);
                        }

                        //Sending user captured data to Remote server
                        sendData.sendData(latitude,longitude,speed,MainActivity.this);
                    }

                }
            }
        };

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {

            case GRANT_PERMISSIONS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //permission granted
                } else {
                    Toast.makeText(MainActivity.this, "Locations Permissions were denied", Toast.LENGTH_LONG);
                }
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        startLocationUpdates();
    }

    private void startLocationUpdates() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
        }


    }


    @Override
    protected void onPause() {
        super.onPause();

        stopLocationUpdate();
    }

    private void stopLocationUpdate() {

        mFusedLocationClient.removeLocationUpdates(locationCallback);
    }

    @Override
    protected void onStart() {
        super.onStart();

        //Checking if GPS is turned on
        int off = 0;
        try {
            off = Settings.Secure.getInt(getContentResolver(), Settings.Secure.LOCATION_MODE);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        if(off==0){
            //GPS is turned of execute preceding method for turning on
            showGPSDisabledDialog();
        }
    }


    // Alert Dialog for turning on GPS
    public void showGPSDisabledDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("GPS Disabled");
        builder.setMessage("Gps is disabled, in order to use the application properly you need to enable GPS of your device");
        builder.setPositiveButton("Enable GPS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent onGPS = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(onGPS);
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        builder.create().show();
    }

}
